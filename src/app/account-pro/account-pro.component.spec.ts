import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountProComponent } from './account-pro.component';

describe('AccountProComponent', () => {
  let component: AccountProComponent;
  let fixture: ComponentFixture<AccountProComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountProComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
