import { Component, OnInit } from '@angular/core';
import { Account } from '../entity/account';
import { AccountService } from '../service/account.service';
import { Transaction } from '../entity/transaction';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  account:Account[] = [];
  newAccount:Account = {"id":"", "name":""};
  selected:Transaction = {};

  constructor(private actService:AccountService ) { }

  ngOnInit() {
    this.actService.findAll().subscribe(data => this.account = data);
  }
}
