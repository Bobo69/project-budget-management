import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccountCurrentComponent } from './account-current/account-current.component';
import { AccountProComponent } from './account-pro/account-pro.component';
import { AccountComponent } from './account/account.component';
import { AppComponent } from './app.component';
import { FormulaireComponent } from './formulaire/formulaire.component';
import { HomepageComponent } from './homepage/homepage.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TransactionComponent } from './transaction/transaction.component';


const routes: Routes = [
  {path: 'home', component: HomepageComponent},
  {path: 'formulaire', component: FormulaireComponent},
  //rajouter un paramètre id à la route
  {path: 'transaction/:id', component: TransactionComponent},
  {path: 'account', component: AccountComponent},
  {path: 'account-pro', component: AccountProComponent},
  {path: 'account-current', component: AccountCurrentComponent},
  {path: 'nav', component: NavBarComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    FormulaireComponent,
    NotFoundComponent,
    NavBarComponent,
    TransactionComponent,
    AccountComponent,
    AccountProComponent,
    AccountCurrentComponent,
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      routes,
    ),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
