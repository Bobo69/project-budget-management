import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from '../entity/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private url = "http://localhost:8080/account"

  constructor(private http:HttpClient) { }

  findAll(): Observable<Account[]> {
    return this.http.get<Account[]>(this.url);
  }

  findById(id:number): Observable<Account> {
    return this.http.get<Account>(this.url + "/" + id);
 }
  add(account:Account): Observable<Account> {
    return this.http.post<Account>(this.url + "/", account);
  }
}
