import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Transaction } from '../entity/transaction';



@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private url = "http://localhost:8080/transaction";

  constructor(private http:HttpClient) { }

  findAll(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(this.url)
   }

  add(transaction:Transaction): Observable<Transaction> {
    return this.http.post<Transaction>(this.url + "/", transaction);
  }
  delete(id:number): Observable<any> {
    return this.http.delete<any>(this.url + "/" + id);
  }
  update(transaction:Transaction): Observable<Transaction> {
    return this.http.put<Transaction>(this.url + "/" + transaction.id, transaction);
  }
}
