import { Component, OnInit } from '@angular/core';
import { Transaction } from '../entity/transaction';
import { TransactionService } from '../service/transaction.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  transaction:Transaction[] = [];
  newTransa:Transaction = {"id":"","date":new Date(), "amount":"", "description":""};
  selected:Transaction = {};
  imgSrc = '../assets/photos/loupe.png';

  constructor(private service:TransactionService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data => this.transaction = data);
  }
  addTransac() {
    this.service.add(this.newTransa).subscribe(() => this.ngOnInit());
  }
  deleteTransac() {
    this.service.delete(this.selected.id).subscribe(() => {this.ngOnInit();this.selected;});
  }
  updateTransac(transaction:Transaction) {
    this.service.update(transaction).subscribe();
  }
}
